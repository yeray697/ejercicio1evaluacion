##### Se introducirán las rutas al fichero con enlaces a imágenes en la red y al fichero con frases para poder realizar su descarga.
##### El fichero de imágenes contendrá en cada línea un enlace a una imagen y el de frases tendrá en cada línea una frase célebre. Pueden ser diferentes el número de imágenes y el de frases.
##### Cuando se pulse el botón de descarga, se mostrarán una a una las imágenes y frases descargadas, de forma automática cada cierto tiempo. Ese tiempo (en segundos) estará almacenado en el fichero /raw/intervalo.txt.
##### Además, se añadirán al fichero errores.txt situado en un servidor web (en el mismo equipo o en Internet) los errores que se hayan producido:
##### -No se puede descargar el fichero de imágenes o el de frases
##### -No se puede descargar alguna imagen, etc.
##### Por cada error producido, se añadirá al fichero errores.txt una línea con la ruta al archivo que se quiere descargar, la fecha y hora de acceso y la causa del error (fallo en el servidor web, no existe el fichero, . . . ).
###### .
###### .
###### .
###### La aplicación permite rotar la pantalla sin perder lo datos o reiniciar la conexión
###### Controlo que al quitar la aplicación pare el hilo, y que al restaurarla vuelva a restaurar el hilo por donde iba.
###### Esta es la interfaz que tiene:
![](http://yeray697.esy.es/ejerciciospaco/1.png)
###### Al pulsar el botón "Descargar" tratará de descargar el fichero de imágenes y el de frases.
###### Al obtener el fichero de imágenes, se comprueba que el contenido sean direcciones de imagen válidas
###### Si da error, se muestra un Toast y se sube al servidor.
###### De error o no, después intenta descargar las frases.
###### Si da error, se muestra un Toast y se sube al servidor.
###### De error o no, después muestra las frases y las imágenes, si hay alguna que mostrar, mediante un Thread, cada X tiempo (el tiempo lo coge al cargar la aplicación del fichero de raw/intervalo.txt. Si no lo puede cargar debido a un error, lo hace cada 2 segundos por defecto).
###### Cuando se muestran, indica cuál es la que se está mostrando (por ejemplo Imagen 1 de 5).
![](http://yeray697.esy.es/ejerciciospaco/2.png)
###### Mientras carga la imagen, aparece esta imagen:
![](http://yeray697.esy.es/ejerciciospaco/3.png)
###### En caso de que la dirección de la imagen no exista, muestra el error en un toast, cambia la imagen, y sube el error al servidor
![](http://yeray697.esy.es/ejerciciospaco/4.png)
###### Al pulsar el botón "Mostrar errores" abre el navegador con la dirección donde se guarda el archivo de errores
![](http://yeray697.esy.es/ejerciciospaco/5.png)