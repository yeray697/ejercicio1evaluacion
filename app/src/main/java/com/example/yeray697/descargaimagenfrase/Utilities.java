package com.example.yeray697.descargaimagenfrase;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

/**
 * Static class with useful methods
 */

public class Utilities {
    /**
     * Read and return the input of a resource file
     * @param context Context of the application
     * @param resId Resource id file
     * @return Return an integer that is the input of the file
     */
    public static int readFromRawTxt(Context context, int resId)
    {
        InputStream inputStream = context.getResources().openRawResource(resId);
        InputStreamReader inputreader = new InputStreamReader(inputStream);
        BufferedReader buffreader = new BufferedReader(inputreader);
        String line;
        StringBuilder text = new StringBuilder();

        try {
            while (( line = buffreader.readLine()) != null) {
                text.append(line);
            }
        } catch (IOException e) {
            return 2000;
        }
        return Integer.parseInt(text.toString());
    }

    /**
     * Method that check if the mobile has internet
     * @param context Context of the application
     * @return Return true if there is internet
     */
    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();
        if (networkInfo != null && networkInfo.isConnected())
            return true;
        else
            return false;
    }

    /**
     * Method that sends to an url a message
     * @param context Context of the application
     * @param error Message that will be send to server
     * @param url Php's url used to upload the String
     */
    public static void sendError(final Context context, String error, String url) {
        Calendar calendar = Calendar.getInstance();
        RequestParams params = new RequestParams();
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy hh:mm:ss");
        params.put("error", "Archivo: "+url+"\nError: "+error+"\nFecha: "+simpleDateFormat.format(calendar.getTime())+"\n");
        RestClient.post("http://yeray697.esy.es/images/errors/uploadError.php", params, new AsyncHttpResponseHandler() {
        //params.put("contenido", "Archivo: "+url+"\nError: "+error+"\nFecha: "+simpleDateFormat.format(calendar.getTime())+"\n");
        //RestClient.post("https://errores.000webhostapp.com/upload.php", params, new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {

            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                Toast.makeText(context, "No se ha podido subir el error al servidor", Toast.LENGTH_SHORT).show();
            }
        });
    }
}
