package com.example.yeray697.descargaimagenfrase;

/**
 * Class with a thread that sleep and call onTick.tick() every $time milliseconds;
 */

public class ThreadTick extends Thread {

    boolean active = false;
    int time;
    IOnTick onTick;

    interface IOnTick{
        void tick();
    }

    public void startThread() {
        active = true;
        start();
    }

    public ThreadTick(int time, IOnTick onTick){
        this.time = time;
        this.onTick = onTick;
    }
    public void stopThread(){
        active = false;
        interrupt();
    }
    @Override
    public void run() {
        while (active) {
            onTick.tick();
            try {
                sleep(time);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
