package com.example.yeray697.descargaimagenfrase;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.squareup.picasso.Picasso;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import cz.msebera.android.httpclient.Header;

public class MainActivity extends AppCompatActivity {
    EditText etDownloadImage, etDownloadPhrase;
    Button btDownload;
    Button btShowErrors;
    ImageView ivImage;
    TextView tvNumberImage, tvPhrase, tvNumberPhrase;

    ArrayList<String> imageUrls;
    ArrayList<String> phrases;
    int cursorImage;
    int cursorPhrase;

    ThreadTick recorrerThread;
    boolean hiloCorriendo;
    ThreadTick.IOnTick iOnTick;
    int intervalo;

    ProgressDialog dialog;


    @Override
    protected void onPause() {
        super.onPause();
        if (recorrerThread != null)
            recorrerThread.stopThread();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (recorrerThread != null && hiloCorriendo) {
            try{
                recorrerThread.startThread();
            }catch(Exception ex){
                recorrerThread = new ThreadTick(intervalo,iOnTick);
                recorrerThread.startThread();
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        intervalo = Utilities.readFromRawTxt(this,R.raw.intervalo);
        iOnTick = new ThreadTick.IOnTick() {
            @Override
            public void tick() {

                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        setImageAndPhrase();
                    }
                });
            }
        };
        //Setting phrases and images
        imageUrls = new ArrayList<>();
        phrases = new ArrayList<>();
        cursorPhrase = 0;
        cursorImage = 0;
        //Setting thread
        recorrerThread = new ThreadTick(intervalo, iOnTick);
        //Setting views
        etDownloadImage = (EditText) findViewById(R.id.etDownloadImages);
        etDownloadPhrase = (EditText) findViewById(R.id.etDownloadPhrases);
        btDownload = (Button) findViewById(R.id.btDownload);
        btShowErrors = (Button) findViewById(R.id.btShowErrors);
        ivImage = (ImageView) findViewById(R.id.ivImage);
        tvNumberImage = (TextView) findViewById(R.id.tvNumberImage);
        tvPhrase = (TextView) findViewById(R.id.tvPhrase);
        tvNumberPhrase = (TextView) findViewById(R.id.tvNumberPhrase);
        //Setting listeners
        btDownload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Utilities.isNetworkAvailable(MainActivity.this)) {
                    String imageUrl = etDownloadImage.getText().toString();
                    final String phraseUrl = etDownloadPhrase.getText().toString();
                    if (!TextUtils.isEmpty(imageUrl)) {
                        if (!TextUtils.isEmpty(imageUrl)) {
                            loadImages(imageUrl, phraseUrl);
                        } else {
                            etDownloadPhrase.setError("La url no puede estar vacía");
                        }
                    } else {
                        etDownloadImage.setError("La url no puede estar vacía");
                    }
                } else {
                    Toast.makeText(MainActivity.this, "No tienes conexión a internet", Toast.LENGTH_SHORT).show();
                }
            }
        });
        btShowErrors.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://yeray697.esy.es/images/errors/errors.txt"));
                startActivity(browserIntent);
            }
        });

        etDownloadImage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etDownloadImage.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
        etDownloadPhrase.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                etDownloadPhrase.setError(null);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        hiloCorriendo = false;
    }

    /**
     * Method that tries to load images from an url, and after that, call another method to load phrases
     * @param imageUrl Url where images are located
     * @param phraseUrl Url where phrases are located
     */
    private void loadImages(final String imageUrl, final String phraseUrl) {
        dialog = new ProgressDialog(MainActivity.this);
        RestClient.get(imageUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.setCancelable(false);
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setMessage("Descargando imágenes...");
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                imageUrls = new ArrayList<String>();
                CharSequence seq2 = new String(responseBody, Charset.forName("UTF-8"));
                String pattern = "(http(s?):/)(/[^/]+)+" + ".(?:jpg|gif|png)";
                boolean matches;
                for (String q : seq2.toString().split("\n| ")) {
                    matches = q.matches(pattern);
                    if (matches)
                        imageUrls.add(q);
                }
                if (imageUrls != null && imageUrls.size() > 0){
                    Toast.makeText(MainActivity.this, "Se han descargado las imágenes correctamente", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Se ha descargado correctamente el fichero, pero no había ninguna imagen válida", Toast.LENGTH_SHORT).show();
                }
                dialog.dismiss();
                loadPhrases(phraseUrl);
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {

                String errorMessage = "Error al descargar las imágenes:" + statusCode + " " + error.getMessage();
                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();
                Utilities.sendError(MainActivity.this, errorMessage,imageUrl);
                if (imageUrls != null)
                    imageUrls.clear();
                dialog.dismiss();
                loadPhrases(phraseUrl);
            }
        });
    }

    /**
     * Method that tries to load phrases from an url, and after that, call another method to show phrases and images
     * @param phraseUrl Url where phrases are located
     */
    private void loadPhrases(final String phraseUrl){
        dialog = new ProgressDialog(MainActivity.this);
        RestClient.get(phraseUrl, new AsyncHttpResponseHandler() {
            @Override
            public void onStart() {
                super.onStart();
                dialog.setCancelable(false);
                dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
                dialog.setMessage("Descargando frases...");
                dialog.show();
            }

            @Override
            public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                phrases = new ArrayList<String>();
                CharSequence seq2 = new String(responseBody, Charset.forName("UTF-8"));
                for (String q : seq2.toString().split("\n"))
                    phrases.add(q);
                if (phrases != null && phrases.size() > 0){
                    Toast.makeText(MainActivity.this, "Se han descargado las frases correctamente", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(MainActivity.this, "Se ha descargado correctamente, pero no había ninguna frase", Toast.LENGTH_SHORT).show();
                }
                loaded();
                dialog.dismiss();
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                String errorMessage = "Error al descargar las frases:" + statusCode + " " + error.getMessage();
                Toast.makeText(MainActivity.this, errorMessage, Toast.LENGTH_SHORT).show();

                Utilities.sendError(MainActivity.this,errorMessage,phraseUrl);
                dialog.dismiss();
                if (phrases != null)
                    phrases.clear();
                loaded();
            }
        });
    }

    /**
     * Method called when images and phrases are loaded
     */
    private void loaded() {
        recorrerThread.stopThread();
        cursorImage = 0;
        cursorPhrase = 0;
        if (phrases == null || phrases.size() == 0) {
            tvNumberPhrase.setText("");
            tvPhrase.setText("No hay ninguna frase");
        }
        if (imageUrls == null|| imageUrls.size() == 0) {
            tvNumberImage.setText("No hay ninguna imagen");
            ivImage.setImageDrawable(null);
            ivImage.setVisibility(View.GONE);
        } else
            ivImage.setVisibility(View.VISIBLE);
        if ((phrases != null && phrases.size() != 0 ) || (imageUrls != null && imageUrls.size() != 0)) {
            try{
                recorrerThread.startThread();
            }catch(Exception ex){
                recorrerThread.stopThread();
                recorrerThread = new ThreadTick(intervalo,iOnTick);
                recorrerThread.startThread();
            }
            hiloCorriendo = true;
        }
    }

    /**
     * Method that set the image and phrase (if it is possible), and increment counters
     */
    private void setImageAndPhrase() {
        if (phrases != null && phrases.size() != 0) {
            tvNumberPhrase.setText("Frase " + ((cursorPhrase % phrases.size()) + 1) + " de " + phrases.size());
            tvPhrase.setText(phrases.get((cursorPhrase % phrases.size())));
            cursorPhrase++;
        }
        if (imageUrls != null && imageUrls.size() != 0) {
            tvNumberImage.setText("Imagen " + ((cursorImage % imageUrls.size()) + 1) + " de " + imageUrls.size());
            Picasso.Builder builder = new Picasso.Builder(this);
            builder.listener(new Picasso.Listener()
            {
                @Override
                public void onImageLoadFailed(Picasso picasso, Uri uri, Exception exception)
                {
                    String error = "Error al mostrar la imagen: " + exception.getMessage();
                    Toast.makeText(MainActivity.this, error, Toast.LENGTH_SHORT).show();
                    Utilities.sendError(MainActivity.this,error,imageUrls.get(cursorImage % imageUrls.size()));
                }
            });
            builder.build().load(imageUrls.get(cursorImage % imageUrls.size()))
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.error)
                    .into(ivImage);
            cursorImage ++;
        }
    }



}